import Bootloader from './Bootloader.js';

const config = {
    title: "simpletest",
    version: "0.0.1",
    type: Phaser.AUTO,
    scale: {
        parent: "phaser_container",
        width: 1920,
        height: 1080,
        mode: Phaser.Scale.FIT,
        autoCenter: Phaser.Scale.CENTER_BOTH
    },
    backgroundColor: "#fff",
    pixelArt: false,
    scene: [
        Bootloader
    ]
};

new Phaser.Game(config);