
class Bootloader extends Phaser.Scene {
    constructor() {
        super({key: 'Bootloader' });
        
    }

    preload() {
        console.log('Bootloader');
        this.load.setPath('./assets/');
        localStorage.clear();
        this.veces = 0

        this.load.audio('btn', 'sounds/beep.wav')
        this.load.audio('sucess', 'sounds/success.mp3')
        this.load.audio('bgMusic', 'sounds/bg_music.mp3')
 
        this.load.image('backRefer', 'backRefer.png');
        this.load.image('body', 'body.png');
        this.load.image('intro', 'instruction_box.png');
        this.load.image('start_button', 'start_button.png');

        this.load.image('baseLeftLeg', 'collisionElement.png');
        this.load.image('baseRightLeg', 'collisionElement.png');

        this.load.image('baseLeftArm', 'collisionElement.png');
        this.load.image('baseRightArm', 'collisionElement.png');

        this.load.image('baseHelmet', 'collisionElement.png');
        this.load.image('baseChestShield', 'collisionElement.png');

        this.load.image('collisionerLeftLeg', 'collisionElement.png');
        this.load.image('collisionerRightLeg', 'collisionElement.png');
        this.load.image('collisionerchestShield', 'collisionElement.png');
        this.load.image('collisionerLeftArm', 'collisionElement.png');
        this.load.image('collisionerRightArm', 'collisionElement.png');
        this.load.image('collisionerHelmet', 'collisionElement.png');

        this.load.image('leftLeg', 'leftLeg.png');
        this.load.image('rightLeg', 'rightLeg.png');
        this.load.image('chestShield', 'chestShield.png');
        this.load.image('leftArm', 'leftArm.png');
        this.load.image('rightArm', 'rightArm.png');
        this.load.image('helmet', 'helmet.png');

        this.load.image('intro', 'instruction_box.png');
        this.load.image('Instructions', 'start_overlay-box-large.png');
        this.load.image('start_button', 'start_button.png');

        this.load.image('white', 'white.png');
        this.load.image('last', 'lastOne.png');
        this.load.image('out', 'out.png');

        this.load.on('complete', () => {
            console.log('Load complete');
        });
    }

    dragAndCollision (element,baseLeftLeg,leftLeg,collisionerLeftLeg,name) {
        element.setSize(leftLeg.width, leftLeg.height);
        element.name = name;

        element.setInteractive();

        this.input.setDraggable(element);

        element.on('pointerover', function () {

            leftLeg.setTint(0x44ff44);

        });

        element.on('pointerout', function () {

            leftLeg.clearTint();

        });


        this.input.on('drag', function (pointer, gameObject, dragX, dragY) {

            gameObject.x = dragX;
            gameObject.y = dragY;


        });

        this.input.on('dragend', function (pointer, gameObject, dragX, dragY) {
           

            let factor = 30;
            let collisionerX = Math.round(collisionerLeftLeg.parentContainer.x + collisionerLeftLeg.x)
            let collisionerY = Math.round(collisionerLeftLeg.parentContainer.y + collisionerLeftLeg.y)

            let baseX = Math.round(baseLeftLeg.x)
            let baseY = Math.round(baseLeftLeg.y)

            if ((collisionerX > baseLeftLeg.x - factor && collisionerX < baseLeftLeg.x + factor) && (collisionerY > baseLeftLeg.y - factor && collisionerY < baseLeftLeg.y + factor) ) {
                localStorage.setItem(element.name, "true");
            } else {
                localStorage.setItem(element.name, "false");
            }

        });
    }


    create() {
        
        const backRefer = this.add.image(this.scale.width / 2, this.scale.height / 2, 'backRefer');
        backRefer.alpha=.5;

        this.title = this.add.text(160, 20, 'J.Ramón Calvo: Front End Developer', { fontFamily: 'Arial', fontSize: 24, color: '#000' });
        this.title2 = this.add.text(1500, 20, 'Easy mode', { fontFamily: 'Arial', fontSize: 24, color: '#000' });

        const baseLeftLeg = this.add.sprite(600, 513, 'baseLeftLeg');
        const baseRightLeg = this.add.sprite(736, 510, 'baseRightLeg');
        const baseLeftArm = this.add.sprite(550, 305, 'baseLeftArm');
        const baseRightArm = this.add.sprite(793, 305, 'baseRightArm');
        const baseHelmet = this.add.sprite(673, 260, 'baseHelmet');

        this.body = this.add.image(this.scale.width / 2, this.scale.height / 2, 'body')
        this.body.x = 670; this.body.y = 405;

        const baseChestShield = this.add.sprite(670, 350, 'baseChestShield');

        this.intro = this.add.image(this.scale.width / 2, this.scale.height / 2, 'intro')
        this.intro.x = 1370; this.intro.y = 155;


        const collisionerLeftLeg = this.add.image(0, -170, 'collisionerLeftLeg');
        const leftLeg = this.add.image(0, 0, 'leftLeg');
        const leftLegContainer = this.add.container(1032, 553, [ leftLeg, collisionerLeftLeg ]);

        const rightLeg = this.add.image(0, 0, 'rightLeg');
        const collisionerRightLeg = this.add.image(-20, -170, 'collisionerRightLeg');
        const rightLegContainer = this.add.container(1177, 553, [ rightLeg, collisionerRightLeg ]);
        

        const leftArm = this.add.image(0, 0, 'leftArm');
        const collisionerLeftArm = this.add.image(50, -120, 'collisionerLeftArm');
        const leftArmContainer = this.add.container(1570, 470, [ leftArm, collisionerLeftArm ]);

        const rightArm = this.add.image(0, 0, 'rightArm');
        const collisionerRightArm = this.add.image(-50, -120, 'collisionerRightArm');
        const rightArmContainer = this.add.container(1695, 470, [ rightArm, collisionerRightArm ]);

        const helmet = this.add.image(0, 0, 'helmet');
        const collisionerHelmet = this.add.image(0, 60, 'collisionerHelmet');
        const helmetContainer = this.add.container(1333, 654, [ helmet, collisionerHelmet ]);
        
        const chestShield = this.add.image(0, 0, 'chestShield');
        const collisionerchestShield = this.add.image(0, 60, 'collisionerchestShield');
        const chestShieldContainer = this.add.container(1360, 406, [ chestShield, collisionerchestShield ]);

        this.dragAndCollision(leftLegContainer,baseLeftLeg,leftLeg,collisionerLeftLeg,'leftLegContainer',)
        this.dragAndCollision(rightLegContainer,baseRightLeg,rightLeg,collisionerRightLeg,'rightLegContainer')
        this.dragAndCollision(leftArmContainer,baseLeftArm,leftArm,collisionerLeftArm,'leftArmContainer')
        this.dragAndCollision(rightArmContainer,baseRightArm,rightArm,collisionerRightArm,'leftArmContainer')
        this.dragAndCollision(helmetContainer,baseHelmet,helmet,collisionerHelmet,'helmetContainer')
        this.dragAndCollision(chestShieldContainer,baseChestShield,chestShield,collisionerchestShield,'chestShieldContainer')

        this.intro = this.add.image(this.scale.width / 2, this.scale.height / 2, 'intro')
        this.intro.x = 1370; this.intro.y = 155;

        this.Instructions = this.add.image(this.scale.width / 2, this.scale.height / 2, 'Instructions')
        this.start_button = this.add.image(this.scale.width / 2, this.scale.height / 2, 'start_button')
        this.Instructions.x = 0; this.Instructions.y = 0;
        this.start_button.x = -143; this.start_button.y = 315;

        let audioBtn = this.sound.add('btn', { loop: false })
        let bgMusic = this.sound.add('bgMusic', { loop: true })
        bgMusic.play()
        bgMusic.volume=.2
        
        const IntructionsContainer = this.add.container(1920/1.8, 1080/2, [ this.Instructions, this.start_button ]);

        this.start_button.setInteractive()
            .on(Phaser.Input.Events.GAMEOBJECT_POINTER_DOWN, () =>{

                IntructionsContainer.x = 5000
                audioBtn.play()
                backRefer.alpha=1;
            })
        this.start_button.setInteractive()
            .on(Phaser.Input.Events.GAMEOBJECT_POINTER_OVER, () =>{
                this.start_button.setTint(0x00FFFF);
            })

        this.start_button.setInteractive()
            .on(Phaser.Input.Events.GAMEOBJECT_POINTER_OUT, () =>{
                this.start_button.clearTint();
            })

        this.White = this.add.image(this.scale.width / 2, this.scale.height / 2, 'white')
        this.White.alpha=0
        this.last = this.add.image(5000, 5000, 'last').setScale(.5)

        this.out = this.add.image(1920/2, 1080/2, 'out').setScale(.3)
        this.out.alpha=0

    }
    update() {

        if (localStorage.getItem('leftLegContainer') === 'true' && localStorage.getItem('rightLegContainer') === 'true' && localStorage.getItem('leftArmContainer') === 'true' && localStorage.getItem('leftArmContainer') === 'true' && localStorage.getItem('helmetContainer') === 'true' && localStorage.getItem('chestShieldContainer') === 'true' ) {
            this.veces++
            if (this.veces <= 1) {

                let Sucess = this.sound.add('sucess', { loop: false })
                Sucess.play()

                let waiting = 4000
                this.last.x = 1920/2;
                this.last.y = 1080/2;

                this.tweens.add({
                    targets: this.White,
                    duration: 1000,
                    alpha: .6,
                    ease: 'Power1'
                })
                
                this.tweens.add({
                    targets: this.last,
                    duration: 1000,
                    scale: 1.1,
                    ease: 'Power1'
                })

                this.tweens.add({
                    targets: this.White,
                    duration: 1000,
                    alpha: .6,
                    ease: 'Power1'
                })

                this.tweens.add({
                    targets: this.White,
                    delay: waiting,
                    duration: 1000,
                    alpha: 1,
                    ease: 'Power1'
                })

                this.tweens.add({
                    targets: this.last,
                    delay: waiting,
                    duration: 1000,
                    alpha: 0,
                    ease: 'Power1'
                })

                this.tweens.add({
                    targets: this.out,
                    duration: 1000,
                    delay: waiting,
                    alpha: 1,
                    ease: 'Power1'
                })

                this.White.setInteractive()
                    .on(Phaser.Input.Events.GAMEOBJECT_POINTER_DOWN, () =>{
                    location.reload();
                    })
            }
        } else {
            this.veces = 0
        }
    }
}
export default Bootloader;